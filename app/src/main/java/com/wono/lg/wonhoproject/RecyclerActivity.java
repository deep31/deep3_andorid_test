package com.wono.lg.wonhoproject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class RecyclerActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    RecyclerAdapter recyclerAdapter;
    ArrayList<TelVO> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);

        recyclerView = findViewById(R.id.rc_li);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false);

        RecyclerView.LayoutManager layoutManager1 = new GridLayoutManager(this,2);
        recyclerView.setLayoutManager(layoutManager1);

        data = new ArrayList<>();
        data.add(new TelVO("박원호","010-0000"));
        data.add(new TelVO("박원호","010-0000"));
        data.add(new TelVO("박원호","010-0000"));
        data.add(new TelVO("박원호","010-0000"));
        data.add(new TelVO("박원호","010-0000"));
        data.add(new TelVO("박원호","010-0000"));
        data.add(new TelVO("박원호","010-0000"));
        data.add(new TelVO("박원호","010-0000"));
        data.add(new TelVO("박원호","010-0000"));
        data.add(new TelVO("박원호","010-0000"));

        recyclerAdapter = new RecyclerAdapter(data);
        recyclerView.setAdapter(recyclerAdapter);


    }
}
