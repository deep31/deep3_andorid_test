package com.wono.lg.wonhoproject;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.wono.lg.wonhoproject.Movie.MovieList;

import java.util.HashMap;
import java.util.Map;

public class Movieapi extends AppCompatActivity {

    EditText edt_addr;
    TextView tv_movie;
    Button btn_send;

    static RequestQueue requestQueue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movieapi);
        edt_addr = findViewById(R.id.edt_addr);
        tv_movie = findViewById(R.id.tv_movie);
        btn_send = findViewById(R.id.btn_req);

        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //makeRequest();
                testJson();
            }
        });
        if(requestQueue == null){
            requestQueue = Volley.newRequestQueue(getApplicationContext());
        }

    }

    public void testJson(){
        String url = "http://172.30.1.56:8085/Json/Ex01.jsp";
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                println(response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        request.setShouldCache(false);
        requestQueue.add(request);
    }

    private void makeRequest() {
        String url = "http://www.kobis.or.kr/kobisopenapi/webservice/rest/boxoffice/searchDailyBoxOfficeList.json?key=430156241533f1d058c603178cc3ca0e&targetDt=20200401";
        //String url = "http://172.30.1.56:8085/Json/Ex01.jsp";
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //println("응답 : " + response);
                processResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                println("에러 : " +error.getMessage());
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                return params;
            }
        };
        request.setShouldCache(false);
        requestQueue.add(request);
        println("요청보냄");
    }


    public void println(String data){
        tv_movie.append(data+"\n");
    }

    public void processResponse(String response){
        Gson gson = new Gson();
        MovieList movieList = gson.fromJson(response, MovieList.class);

        println("영화정보 수 : " +movieList.boxOfficeResult.dailyBoxOfficeList.size());
        tv_movie.append(movieList.boxOfficeResult.dailyBoxOfficeList.get(0).rank);


      //  tv_movie.append(dto.getName());
    }
}
