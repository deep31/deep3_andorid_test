package com.wono.lg.wonhoproject;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

public class Q2_Thread extends AppCompatActivity {
    TextView tv2;
    int cnt =3;
    int cnt_copy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_q2__thread);
        tv2 = findViewById(R.id.tv2);

        cnt_copy = cnt;
        CntRunnable cntRunnable = new CntRunnable();
        Thread thread = new Thread(cntRunnable);
        thread.start();



    }
    public class CntRunnable implements Runnable{

        boolean isRunning = true;
        @Override
        public void run() {
            while(isRunning){

                handler.post(runnable);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }


                if(cnt<0){
                    isRunning = false;
                }

            }
        }
    }
    Handler handler = new Handler();
    Runnable runnable = new Runnable() {
        @Override
        public void run() {

            tv2.setText(cnt +"");
            cnt--;
            if(cnt == 0){
                Intent intent = new Intent(Q2_Thread.this, Q2_Next.class);
                intent.putExtra("cnt",cnt_copy);
                startActivity(intent);
                finish();
            }
        }
    };
}
