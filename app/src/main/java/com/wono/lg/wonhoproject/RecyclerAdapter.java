package com.wono.lg.wonhoproject;

import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder> {
    private ArrayList<TelVO> data;
    public static class MyViewHolder extends RecyclerView.ViewHolder{

        TextView tv_name, tv_tel;
        ImageView img;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_tel = itemView.findViewById(R.id.tv_tel);
            img = itemView.findViewById(R.id.img_people);
        }
    }

    public RecyclerAdapter(ArrayList<TelVO> data){
        this.data = data;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.re_li1,parent,false);
        MyViewHolder vh = new MyViewHolder(layout);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.tv_name.setText(data.get(position).getName());
        holder.tv_tel.setText(data.get(position).getTel());
    }



    @Override
    public int getItemCount() {
        return data.size();
    }
}
