package com.wono.lg.wonhoproject;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.TextView;

public class ThreadActivity extends AppCompatActivity {

    //쓰레드란?
    //무언가를 동시에 실행하기 위해 사용
    //예를 들어보면 노래를 들으면서 타이핑을 하고 있다 노래가 끝나고 내가 치는 타이핑이 쳐지는 것이 아니라 노래는 노래대로 타이핑은 타이핑대로 따로감
    // 이게 쓰레드인데 간혹 인간의 움직임을 생각해서 "선생님 그러면 막 노래가 끊기거나 타이핑이 느려야하는 거 아닙니까" 라는 질문을 하시는분들이 계시는데
    //생각보다 컴퓨터는 무지막지하게 빠릅니다 근데 위와 유사하게 너무 많은 일처리를 하거나 컴퓨터 사양이 안좋으면 중간중간 노래가 끊기는 것이 맞다

    //쓰레드 사용 방법은 2가지가 있다 왜 2가지가 있을까?
    // Thread라는 클래스를 상속받기, Runnable이라는 인터페이스를 구현받기
    // 자바 상속의 3가지 특징중에 한 특징때문에 그렇다 --> 자바 상속은 다중상속을 지원하지 않는다
    // A라는 클래스를 상속받으면서 쓰레드를 사용하고 싶다면?? --> 다중상속을 지원하고 있지 않기 때문에 쓰레드 사용이 어려움
    // 그러므로 Runnable이라는 인터페이스가 존재 --> 2가지 다 배워볼 예정
    TextView txt1,txt2;
    CntHandler handler = new CntHandler();
    int cnt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thread);


        // 1.thread 클래스 사용


        txt1 = findViewById(R.id.txt_cnt1);
        txt2 = findViewById(R.id.txt_cnt2);
        // 쓰레드 클래스를 만들어서 사용(내부클래스로 만들어서 사용해봅시다)
        CountThread thread = new CountThread();
        //쓰레드 시작 문구
        thread.start();

        //runnable 독단적으로 start할수가 없기 때문에 thread객체 생성 시 매개변수로 넣어주어 시작!
        CntRunnable cntRunnable = new CntRunnable();
        Thread thread1 = new Thread(cntRunnable);
        thread1.start();

    }

    public class CntHandler extends Handler {
        @Override
        public void handleMessage(@NonNull Message msg) {
            Bundle bundle = msg.getData();
            int cnt = bundle.getInt("cnt");
            txt1.setText(cnt + "");
        }
    }

    private class CountThread extends Thread {
        @Override
        public void run() {

            for (int i = 0; i <= 10; i++) {
                // handler없이 작동 --> 오류나는게 정상입니다
                //오류나는 이유!
                // 기본적으로 프로젝트에는 메인쓰레드가 존재
                //메인쓰레드가 UI를 독단적으로 점유 하고 있음 하지만 개인이 만든 쓰레드가 UI를 변경하려 할 시에는 데드락이 걸려버림
                //그러므로 메인쓰레드가 UI를 처리할 수 있도록 메세지를 전달 해 주는 Handler를 사용
                // 핸들러를 통하여 메세지를 전달하고 메인 쓰레드가 처리하여 UI개선
                //txt1.setText(i+"");
                Message message = handler.obtainMessage();
                Bundle bundle = new Bundle();
                bundle.putInt("cnt", i);
                message.setData(bundle);
                handler.sendMessage(message);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    // Runnable사용
    //인터페이스의 설명도 부가적으로 하기
    public class CntRunnable implements Runnable {
        boolean isRunning = true;
        @Override
        public void run() {
            while (isRunning) {

                if(cnt>=3){
                    isRunning = false;
                }
                run_handler.post(runnable);
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            txt2.setText(cnt++ +"");
        }
    };
    Handler run_handler = new Handler();

}
