package com.wono.lg.wonhoproject;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Q_Thread extends AppCompatActivity {
    TextView tv;
    Button btn;
    int cnt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_q__thread);
        tv = findViewById(R.id.tv1);
        btn = findViewById(R.id.btn_start);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CntRunnable cntRunnable = new CntRunnable();
                Thread thread = new Thread(cntRunnable);
                thread.start();
            }
        });
    }

    public class CntRunnable implements Runnable{

        boolean isRunning = true;
        @Override
        public void run() {
            while(isRunning){


                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                handler.post(runnable);

                if(cnt>10){
                    isRunning = false;
                }

            }
        }
    }
    Handler handler = new Handler();
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            tv.setText(cnt++ +"");
        }
    };
}
