package com.wono.lg.wonhoproject;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.wono.lg.wonhoproject.view.Monday;
import com.wono.lg.wonhoproject.view.Tuesday;
import com.wono.lg.wonhoproject.view.Wednesday;

public class PageAdapter extends FragmentPagerAdapter {

    int tabCnt;
    public PageAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
        tabCnt = behavior;

    }

    @NonNull
    @Override
    public Fragment getItem(int position) {

        if(position == 0){
            Monday monday = new Monday();
            return monday;

        }else if(position ==1){
            return new Tuesday();
        }else if(position ==2){
            return new Wednesday();
        }else{
            return  null;
        }

    }

    @Override
    public int getCount() {
        return tabCnt;
    }
}
