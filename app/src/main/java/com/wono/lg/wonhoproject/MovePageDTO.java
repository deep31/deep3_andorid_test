package com.wono.lg.wonhoproject;

public class MovePageDTO {
    private String name;
    private Class<?> page;

    public MovePageDTO(String name, Class<?> page) {
        this.name = name;
        this.page = page;
    }

    public String getName() {
        return name;
    }

    public Class<?> getPage() {
        return page;
    }
}
