package com.wono.lg.wonhoproject;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    //전화번호 뷰에 이름, 사진 처럼 여러가지가 관리
    //리스트뷰는 껍데기 역할, 어뎁터한테 요청하면 데이터관리, 아이템뷰도 생성

    // item을 추가
    ArrayList<String> items;
    int count = 0;
    // 기본적으로 제공하는 어뎁터가 다양하게 존재하지만 보통은 사용자 정의해서 사용
    // 이 부분은 RecyclerView를 공부 할 시 자세히 사용자 정의 어뎁터를 만들어 보기

    ArrayAdapter<String> adapter;

    //Map<String, Class<?>> movePage = new LinkedHashMap<>();
    ArrayList<MovePageDTO> dto;
    ListView lv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        items = new ArrayList<>();
        //ArrayAdapter를 만들기 위해서 context, xml레이아웃,데이터 3개의 매개변수 필요
        // android.R.layout에서 기본적으로 제공하는 simple_list_item_1작성
        // RecyclerView 할 때 사용자정의 xml구성해보는 것도 함께 할 예정
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,items);

        dto = new ArrayList<>();
        lv = findViewById(R.id.lvPage);
        lv.setAdapter(adapter);

        dto.add(new MovePageDTO("쓰레드",ThreadActivity.class));
        dto.add(new MovePageDTO("Q1_쓰레드",Q_Thread.class));
        dto.add(new MovePageDTO("Q2_쓰레드",Q2_Thread.class));

        dto.add(new MovePageDTO("Ex_Intent",MemberActivity.class));
        dto.add(new MovePageDTO("Ex_Fragment", Ex_Fragment.class));
        dto.add(new MovePageDTO("Ryan_Fragment", Q_Fragment.class));
        dto.add(new MovePageDTO("Viewpager", ViewpagerActivity.class));
        dto.add(new MovePageDTO("recycler", RecyclerActivity.class));
        dto.add(new MovePageDTO("웹통신", Movieapi.class));
        for(int i =0; i<dto.size(); i++){
            items.add(++count+". "+dto.get(i).getName());
        }

//        movePage.put("쓰레드",ThreadActivity.class);
//        movePage.put("Q_쓰레드",Q_Thread.class);
//        movePage.put("Q2_쓰레드",Q2_Thread.class);
//        for(String key:movePage.keySet()){
//
//            items.add(++count+"."+key);
//
//
//        }

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {


                Intent intent = new Intent(getApplicationContext(), dto.get(i).getPage());
                startActivity(intent);
//                for(String key: movePage.keySet()) {
//                    Intent intent = new Intent(getApplicationContext(), movePage.get(key));
//                    startActivity(intent);
//                }
            }
        });


    }
}
