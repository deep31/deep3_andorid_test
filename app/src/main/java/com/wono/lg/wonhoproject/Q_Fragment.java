package com.wono.lg.wonhoproject;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.wono.lg.wonhoproject.fragment.Frag_Ryan1;
import com.wono.lg.wonhoproject.fragment.Frag_Ryan2;
import com.wono.lg.wonhoproject.fragment.Frag_Ryan3;

public class Q_Fragment extends AppCompatActivity {
    Button btn1,btn2,btn3;
    Frag_Ryan1 frag_ryan1;
    Frag_Ryan2 frag_ryan2;
    Frag_Ryan3 frag_ryan3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_q__fragment);
        btn1 =findViewById(R.id.btn_ra1);
        btn2 = findViewById(R.id.btn_ra2);
        btn3 = findViewById(R.id.btn_ra3);
        frag_ryan1 = new Frag_Ryan1();
        frag_ryan2 = new Frag_Ryan2();
        frag_ryan3 = new Frag_Ryan3();

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSupportFragmentManager().beginTransaction().replace(R.id.frame,frag_ryan1).commit();
            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSupportFragmentManager().beginTransaction().replace(R.id.frame,frag_ryan2).commit();
            }
        });
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSupportFragmentManager().beginTransaction().replace(R.id.frame,frag_ryan3).commit();
            }
        });
    }
}
