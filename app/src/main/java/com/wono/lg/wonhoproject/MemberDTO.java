package com.wono.lg.wonhoproject;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;


public class MemberDTO implements Parcelable {
    private String id;
    private String pw;

    private int age;
    private String tel;

    public MemberDTO(String id, String pw,  int age, String tel) {
        this.id = id;
        this.pw = pw;

        this.age = age;
        this.tel = tel;
    }

    public String getId() {
        return id;
    }

    public String getPw() {
        return pw;
    }



    public int getAge() {
        return age;
    }

    public String getTel() {
        return tel;
    }

    public static Creator<MemberDTO> getCREATOR() {
        return CREATOR;
    }

    protected MemberDTO(Parcel in) {
        id = in.readString();
        pw = in.readString();

        age = in.readInt();
        tel = in.readString();
    }

    public static final Creator<MemberDTO> CREATOR = new Creator<MemberDTO>() {
        @Override
        public MemberDTO createFromParcel(Parcel in) {
            return new MemberDTO(in);
        }

        @Override
        public MemberDTO[] newArray(int size) {
            return new MemberDTO[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(pw);

        parcel.writeInt(age);
        parcel.writeString(tel);
    }
}
