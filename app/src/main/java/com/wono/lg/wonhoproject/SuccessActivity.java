package com.wono.lg.wonhoproject;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class SuccessActivity extends AppCompatActivity {

    TextView txt1, txt2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_success);
        txt1 = findViewById(R.id.textView);
        txt2 = findViewById(R.id.textView2);

        Intent intent = getIntent();
        MemberDTO dto = intent.getParcelableExtra("member");

        txt1.setText(dto.getId()+"("+dto.getAge()+"세)"+"님 환영합니다.");
        txt2.setText(dto.getTel()+"번호로 가입되었습니다.");
    }
}
