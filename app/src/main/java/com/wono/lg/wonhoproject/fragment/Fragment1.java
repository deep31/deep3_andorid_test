package com.wono.lg.wonhoproject.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.wono.lg.wonhoproject.R;

public class Fragment1 extends Fragment {
    TextView txt1;

    Button btn1;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        //inflate의 매개변수는 3개
        // 첫번째는 사용하고자 하는 layout R.layout.fragment1
        // 두번째는 Viewgroup onCreateView의 매개변수인 container를 사용
        // 세번째는 boolean값 무조건 false true를 쓸경우 최종 레이아웃에 중복된 뷰그룹을 생성하게 됨
        View view = inflater.inflate(R.layout.fragment1,container,false);

        txt1 = view.findViewById(R.id.tv_main);
        btn1 = view.findViewById(R.id.btn_main);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment2 fragment2 = new Fragment2();
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.container,fragment2).commit();
            }
        });

        return view;
    }
}
