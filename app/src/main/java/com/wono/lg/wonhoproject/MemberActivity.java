package com.wono.lg.wonhoproject;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MemberActivity extends AppCompatActivity {

    EditText edt1, edt2,edt3,edt4;
    Button btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member);

        edt1 = findViewById(R.id.editText);
        edt2 =findViewById(R.id.editText2);
        edt3 = findViewById(R.id.editText3);
        edt4 = findViewById(R.id.editText4);

        btn = findViewById(R.id.btn_send);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), SuccessActivity.class);
                MemberDTO dto = new MemberDTO(edt1.getText().toString(), edt2.getText().toString(),Integer.parseInt(edt3.getText().toString()),edt4.getText().toString());
                intent.putExtra("member",dto);
                startActivity(intent);
            }
        });


    }
}
