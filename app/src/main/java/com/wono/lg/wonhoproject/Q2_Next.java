package com.wono.lg.wonhoproject;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class Q2_Next extends AppCompatActivity {

    TextView tv_get;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_q2__next);
        tv_get = findViewById(R.id.tv_get);

        Intent intent = getIntent();
        int num = intent.getIntExtra("cnt",0);
        tv_get.setText(num+"초가 지났습니다!");
    }
}
