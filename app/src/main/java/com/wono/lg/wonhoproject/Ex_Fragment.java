package com.wono.lg.wonhoproject;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.wono.lg.wonhoproject.R;
import com.wono.lg.wonhoproject.fragment.Fragment1;
import com.wono.lg.wonhoproject.fragment.Fragment2;

public class Ex_Fragment extends AppCompatActivity {
    Button btn1, btn2;
    Fragment1 fragment1;
    Fragment2 fragment2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ex__fragment);
        btn1 = findViewById(R.id.btn_1);
        btn2 = findViewById(R.id.btn_2);
        fragment1 = new Fragment1();
        fragment2 = new Fragment2();


        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //소스코드를 통해 fragment 추가 하는 방법
                //버튼 클릭마다 만드니 만들지 않는다
               //  Fragment1 fragment1 = new Fragment1();
                // 트랜잭션은 동시에 수행하고자 하는 변경사항의 집합체 주어진 트랜잭션에 대해 수행하고자 하는 모든 변경사항을 설정 마지막에 액티비티에 적용하기 위해서는 commit메소드 호출

                getSupportFragmentManager().beginTransaction().replace(R.id.container,fragment1).commit();
            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSupportFragmentManager().beginTransaction().replace(R.id.container,fragment2).commit();
            }
        });
    }
}
