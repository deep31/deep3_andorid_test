package com.wono.lg.wonhoproject;

public class TelVO {
    private String name;
    private String tel;

    public TelVO(String name, String tel) {
        this.name = name;
        this.tel = tel;
    }

    public String getName() {
        return name;
    }

    public String getTel() {
        return tel;
    }
}
